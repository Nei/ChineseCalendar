#!/bin/sh
set -e

cd "$(dirname "$0")"

echo "Installing Lunar library to" ~

cpio -p -dv ~ <<FILES
.local/share/ChineseCalendar/calendar.js
.local/share/ChineseCalendar/calendarData.js
.local/share/ChineseCalendar/decompressSunMoonData.js
.local/share/ChineseCalendar/eclipse_linksM722-2202.js
.local/share/ChineseCalendar/utilities.js
FILES

if [ ! -e ~/.config/liblunar/holiday.dat ]; then
    echo "Installing Holiday file to" ~

    cpio -p -dv ~ <<FILES
.config/liblunar/holiday.dat
FILES
else
    echo "Not installing" ~/.config/liblunar/holiday.dat "-- existing file"
fi
